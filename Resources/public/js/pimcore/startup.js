pimcore.registerNS("pimcore.plugin.MopeConfigurationBundle");

pimcore.plugin.MopeConfigurationBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.MopeConfigurationBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        var toolbar = pimcore.globalmanager.get("layout_toolbar");

        var perspectiveCfg = pimcore.globalmanager.get("perspective");


        var menuItems = toolbar.ecommerceswMenu;
        if (!menuItems) {
            menuItems = new Ext.menu.Menu({cls: "pimcore_navigation_flyout"});
            toolbar.ecommerceswMenu = menuItems;
        }

        var insertPoint = Ext.get("pimcore_menu_settings");
        if(!insertPoint) {
            var dom = Ext.dom.Query.select('#pimcore_navigation ul li:last');
            insertPoint = Ext.get(dom[0]);
        }
        var settingPanelId = "bundle_mope_settings_config";
        var item = {
            text: t("bundle_shopware_product_config"),
            iconCls: "pimcore_icon_system",
            handler: function () {
                try {
                    pimcore.globalmanager.get(settingPanelId).activate();
                }
                catch (e) {
                    pimcore.globalmanager.add(settingPanelId, new pimcore.plugin.MopeConfigurationBundle.settings.config.panel(settingPanelId));
                }
            }
        };

        // add to menu
        menuItems.add(item);

        // add ecommerce framewokr main menu
        if (menuItems.items.length > 0) {
            this.navEl = Ext.get(
                insertPoint.insertHtml(
                    "afterEnd",
                    '<li id="pimcore_menu_swsettings" class="pimcore_menu_item icon-sw" data-menu-tooltip="' + t('bundle_mope_settings_main_menu') + '"></li>'
                )
            );

            this.navEl.on("mousedown", toolbar.showSubMenu.bind(menuItems));
            pimcore.helpers.initMenuTooltips();
        }
    }
});
if(pimcore.currentuser.admin) {
    var MopeConfigurationBundlePlugin = new pimcore.plugin.MopeConfigurationBundle();
}
