
pimcore.registerNS("pimcore.plugin.MopeConfigurationBundle.settings.config.panel");

pimcore.plugin.MopeConfigurationBundle.settings.config.panel = Class.create({

    /**
     * @var string
     */
    layoutId: "",

    /**
     * @var array
     */
    condition: [],

    /**
     * @var array
     */
    action: [],

    /**
     * panels of open pricing rules
     */
    panels: {},


    /**
     * constructor
     * @param layoutId
     */
    initialize: function(layoutId) {
        this.layoutId = layoutId;

        // load defined conditions & actions
        var _this = this;

        // create layout
        this.getLayout();
    },


    /**
     * activate panel
     */
    activate: function () {
        var tabPanel = Ext.getCmp("pimcore_panel_tabs");
        tabPanel.setActiveItem( this.layoutId );
    },


    /**
     * create tab panel
     * @returns Ext.Panel
     */
    getLayout: function () {

        if (!this.layout) {

            // create new panel
            this.layout = new Ext.Panel({
                id: this.layoutId,
                title: t("bundle_shopware_product_config"),
                iconCls: "pimcore_icon_system",
                border: false,
                layout: "border",
                closable: true,

                items: [
                    this.getUiElements()
                ]
            });

            this.getSavedSettings();

            // add event listener
            var layoutId = this.layoutId;
            this.layout.on("destroy", function () {
                pimcore.globalmanager.remove( layoutId );
            }.bind(this));

            // add panel to pimcore panel tabs
            var tabPanel = Ext.getCmp("pimcore_panel_tabs");
            tabPanel.add( this.layout );
            tabPanel.setActiveItem( this.layoutId );

            // update layout
            pimcore.layout.refresh();
        }

        return this.layout;
    },


    /**
     * @returns Ext.Panel
     */
    getUiElements: function () {
        if (!this.panel) {
            this.panel = new Ext.form.FormPanel({
                region: "center",
                width: 800,
                bodyStyle: 'padding: 10px',
                border: false,
                items: [{
                    xtype: 'textfield',
                    name: 'shopware_api_url',
                    id: 'shopware_api_url',
                    fieldLabel: t("bundle_mope_shopware_rest_url"),
                    allowBlank: false,
                    labelWidth: 135,
                    width: 590
                },{
                    xtype: 'textfield',
                    name: 'shopware_api_user',
                    id: 'shopware_api_user',
                    fieldLabel: t("bundle_mope_shopware_api_user"),
                    allowBlank: false,
                    labelWidth: 135,
                    width: 590
                }, {
                    xtype: 'textfield',
                    name: 'shopware_api_token',
                    id: 'shopware_api_token',
                    fieldLabel: t("bundle_mope_shopware_rest_token"),
                    allowBlank: false,
                    labelWidth: 135,
                    width: 590
                },{
                    xtype: 'checkbox',
                    name: 'shopware_api_sync_stock',
                    id: 'shopware_api_sync_stock',
                    fieldLabel: t("bundle_mope_shopware_rest_sync_stock"),
                    allowBlank: false,
                    labelWidth: 135,
                    width: 590
                },
                    Ext.create('Ext.Button', {
                        text: t("bundle_mope_shopware_save"),
                        renderTo: Ext.getBody(),
                        handler: function() {
                            var shopware_api_url_input = Ext.getCmp('shopware_api_url').getValue();
                            var shopware_api_user_input = Ext.getCmp('shopware_api_user').getValue();
                            var shopware_api_token_input = Ext.getCmp('shopware_api_token').getValue();
                            var shopware_api_sync_stock_input = Ext.getCmp('shopware_api_sync_stock').getValue();

                            if(shopware_api_url_input.length == 0 || shopware_api_user_input.length == 0 || shopware_api_token_input.length == 0) {
                                pimcore.helpers.showNotification(t("error"), t("bundle_mope_shopware_error_empty_values"), "error");
                            } else {
                                Ext.Ajax.request({
                                    url: "saveswsettings",
                                    method: "post",
                                    params: {
                                        shopware_api_url: shopware_api_url_input,
                                        shopware_api_user: shopware_api_user_input,
                                        shopware_api_token: shopware_api_token_input,
                                        shopware_api_sync_stock: shopware_api_sync_stock_input
                                    },
                                    success: function (response) {
                                        var data = Ext.decode(response.responseText);
                                        if (data.success) {
                                            pimcore.helpers.showNotification(t("success"), t("bundle_mope_shopware_saved"), "success");
                                        } else {
                                            pimcore.helpers.showNotification(t("error"), t("bundle_mope_shopware_error_cant_save"), "error", data.message);
                                        }

                                    }.bind(this)
                                });
                            }
                        }
                })]
            });
        }

        return this.panel;
    },

    /**
     * Gets sw settings
     */
    getSavedSettings: function () {
        Ext.Ajax.request({
            url: "getswsettings",
            method: "get",
            success: function(response) {
                var data = Ext.decode(response.responseText);
                if (data.success) {
                    Ext.getCmp('shopware_api_url').setValue(data.data.shopware_api_url);
                    Ext.getCmp('shopware_api_user').setValue(data.data.shopware_api_user);
                    Ext.getCmp('shopware_api_token').setValue(data.data.shopware_api_token);
                    Ext.getCmp('shopware_api_sync_stock').setValue(data.data.shopware_api_sync_stock);

                } else {
                    pimcore.helpers.showNotification(t("error"), t("bundle_mope_shopware_error_cant_get_data"), "error", data.message);
                }

            }.bind(this)
        });
    }

});
